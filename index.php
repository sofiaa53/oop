<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name; // "shaun"
echo "<br> legs: " . $sheep->legs; // 4
echo "<br> cold blooded: " . $sheep->cold_blooded; // "no"

$kodok = new Frog("buduk");
echo "<br><br>Name: " . $kodok->name; // "shaun"
echo "<br> legs: " . $kodok->legs; // 4
echo "<br> cold blooded: " . $kodok->cold_blooded; // "no"
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
echo "<br><br>Name: " . $sungokong->name; // "shaun"
echo "<br> legs: " . $sungokong->legs; // 4
echo "<br> cold blooded: " . $sungokong->cold_blooded; // "no"
$sungokong->yell(); // "Auooo"